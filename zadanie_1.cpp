#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;

int main()
{
    int tab[11];
    int x, n;
    ifstream in;
    ofstream out, out2;
    out.open("liczby.txt");
    out2.open("dane.txt");
    in.open("dane.txt");
    srand(time(NULL));

    cin >> n;
    for (int i = 0; i <= 10; i++)
        tab[i] = 0;
    for (int i = 0; i < n; i++)
    {
        x = rand() % 10 + 1;
        out2 << x << endl;
    }
    while(in >> x)
        tab[x]++;

    for (int i = 1; i<= 10; i++)
        out << i << ": " << tab[i] << endl;
    in.close();
    out.close();
    out2.close();
}
//autorzy: Jakub Czermański i Oskar Sokólski
