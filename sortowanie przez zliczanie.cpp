#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream in;
    in.open("dane.txt");

    const int k = 1000000;
    const int n = 1000;
    int T[n];
    int Tp[n];
    int TPom[k];
    int i, x, j=0;

    while (in >> x)
    {
        T[j] = x;
        j++;
    }
    for(i = 0 ; i < k ; ++i)
        TPom[i] = 0;

    for(i = 0 ; i < n ; ++i)
        ++TPom[T[i]];

    for(i = 1 ; i < k ; ++i)
        TPom[i] += TPom[i-1];

    for(i = n-1 ; i >= 0 ; --i)
        Tp[--TPom[T[i]]] = T[i];

    for (i = 0; i < k ; i++)
        cout << Tp[i] << endl;


    in.close();
}
