#include <iostream>
#include <algorithm>

using namespace std;

int silnia(int n)
{
    if (n == 0)
        return 1;
    else
        return n * silnia(n - 1);
}

class MyString
{
public:
    MyString();
    MyString(string txt);
    int ileAnagramow();
    bool czyAnagramy(MyString *a);
    void piszAnagramy();
    string txt;
};

MyString::MyString()
{

}

MyString::MyString(string txt)
{
    this->txt = txt;
}

int MyString::ileAnagramow()
{
    int n = this->txt.size();
    int tab[26];
    for (int i = 0; i < 26; i++)
        tab[i] = 0;
    for (int i = 0; i < n; i++)
    {
        char c = this->txt[i];
        if (c > 96 && c < 123)
            c -= 'a';
        else if (c > 64 && c < 91)
            c -= 'A';
        else
            continue;
        tab[c]++;
    }
    int mianownik = 1;
    for (int i = 0; i < 26; i++)
        mianownik *= silnia(tab[i]);
    return silnia(n) / (double)mianownik;
}

/*bool MyString::czyAnagramy(string txt, string txt2)
{
    sort(txt.begin(), txt.end());
    sort(txt2.begin(), txt2.end());
    if (txt == txt2)
        return 1;
    else
        return 0;
}*/
bool MyString::czyAnagramy(MyString *a)
{
    int n = this->txt.size();
    int k = a->txt.size();
    int tab[256];
    int tab2[256];
    for (int i = 0; i < 256; i++)
    {
        tab[i] = 0;
        tab2[i] = 0;
    }
    for (int i = 0; i < n; i++)
    {
        tab[this->txt[i]]++;
    }
    for (int i = 0; i < k; i++)
    {
        tab2[a->txt[i]]++;
    }
    for (int i = 0; i < 256; i++)
        if (tab[i] != tab2[i])
            return 0;
    return 1;
}

void MyString::piszAnagramy()
{
    sort(this->txt.begin(), this->txt.end());
    do
    {
        cout << this->txt << " ";
    } while (next_permutation(this->txt.begin(), this->txt.end()));
    cout << endl;
}

int main()
{
    MyString *lol = new MyString;
    lol->txt = "abc";
    cout << lol->ileAnagramow() << endl;
    lol->piszAnagramy();

    MyString *lol2 = new MyString("cba");
    MyString *lol3 = new MyString("CBA");
    cout << lol->czyAnagramy(lol2) << endl;
    cout << lol->czyAnagramy(lol3) << endl;
    delete lol;
    delete lol2;
    delete lol3;
}
